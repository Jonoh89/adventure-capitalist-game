import { useEffect, useState } from "react";

const getMaxHeight = () => Math.min(720, window.innerHeight);

export default function useCurrentHeight() {
  let [height, setHeight] = useState(getMaxHeight());
  useEffect(() => {
    const onResize = () => {
      setHeight(getMaxHeight());
    };

    window.addEventListener("resize", onResize);
    return () => {
      window.removeEventListener("resize", onResize);
    };
  });
  return height;
}
