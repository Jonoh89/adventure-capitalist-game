import { useEffect, useState } from "react";

const getMaxWidth = () => Math.min(1280, window.innerWidth);

export default function useCurrentWidth() {
  let [width, setWidth] = useState(getMaxWidth());
  useEffect(() => {
    const onResize = () => {
      setWidth(getMaxWidth());
    };

    window.addEventListener("resize", onResize);
    return () => {
      window.removeEventListener("resize", onResize);
    };
  });
  return width;
}
