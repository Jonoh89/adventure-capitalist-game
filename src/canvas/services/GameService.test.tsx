import GameService from "../services/GameService";

describe("GameService", () => {
  let gameService: GameService;

  describe("new game", () => {
    beforeEach(() => {
      gameService = new GameService("test");
    });

    it("should have 10 business", () => {
      expect(gameService.businesses.length).toEqual(10);
    });

    it("should give you one lemonade stand", () => {
      expect(gameService.getBusiness("Lemonade Stand").quantity).toEqual(1);
    });

    describe("run business", () => {
      it("should be a function", () => {
        expect(typeof gameService.runBusiness).toEqual("function");
      });

      it("should run the business", () => {
        gameService.runBusiness("Pub");
        expect(gameService.getBusiness("Pub").running).toEqual(true);
      });
    });
  });

  describe("payout", () => {
    beforeEach(() => {
      gameService = new GameService("test");
    });

    it("should add the business payout to the total cash", () => {
      jest
        .spyOn(gameService.getBusiness("Pub"), "payout", "get")
        .mockImplementation(() => 1000);
      gameService.payout("Pub");
      expect(gameService.cash).toEqual(1000);
    });

    it("should emit cashUpdated", () => {
      const stub = jest.fn();
      gameService.once("cashUpdated", stub);
      gameService.payout("Lemonade Stand");
      expect(stub).toHaveBeenCalledWith(1);
    });

    it("should call finishedRunning on the business", () => {
      jest.spyOn(gameService.getBusiness("Pub"), "finishedRunning");
      gameService.payout("Pub");
      expect(gameService.getBusiness("Pub").finishedRunning).toHaveBeenCalled();
    });
  });

  describe("purchase", () => {
    beforeEach(() => {
      gameService = new GameService("test");
    });

    it("should not allow you to purchase if you dont have the cash", () => {
      gameService.purchase("Pub");
      expect(gameService.getBusiness("Pub").quantity).toEqual(0);
    });

    describe("with cash", () => {
      beforeEach(() => {
        gameService.payout("Lemonade Stand");
        gameService.payout("Lemonade Stand");
      });

      it("should allow you to purchase a business if you have the cash", () => {
        gameService.purchase("Lemonade Stand");
        expect(gameService.getBusiness("Lemonade Stand").quantity).toEqual(2);
      });

      it("should update the cash", () => {
        gameService.purchase("Lemonade Stand");
        expect(gameService.getBusiness("Lemonade Stand").quantity).toEqual(2);
        expect(gameService.cash).toEqual(0.8);
      });

      it("should emit cashUpdated", () => {
        const stub = jest.fn();
        gameService.once("cashUpdated", stub);
        gameService.purchase("Lemonade Stand");
        expect(stub).toHaveBeenCalledWith(0.8);
      });
    });
  });

  describe("hireManager", () => {
    beforeEach(() => {
      gameService = new GameService("test", { cash: 100000 });
    });

    it("should hire a manager for the business", () => {
      gameService.hireManager("Lemonade Stand");
      expect(gameService.getBusiness("Lemonade Stand").managed).toEqual(true);
    });

    it("should start running the business if it wasnt already", () => {
      gameService.hireManager("Lemonade Stand");
      expect(gameService.getBusiness("Lemonade Stand").running).toEqual(true);
    });

    it("should update the cash", () => {
      gameService.hireManager("Lemonade Stand");
      expect(gameService.cash).toEqual(99000);
    });
  });
});
