import config from "../../config";
import BusinessModel from "../models/BusinessModel";
import EventEmitter from "eventemitter3";
import { throttle } from "lodash";

export interface RestoreBusinessData {
  name: string;
  managed: boolean;
  quantity: number;
}

export interface RestoreData {
  cash: number;
  business?: RestoreBusinessData[];
}

export default class GameService extends EventEmitter {
  private readonly _businesses: BusinessModel[];
  private _user: string;
  private _cash: number = 0;
  private _payoutThrottledSave: Function;

  constructor(user: string, restoreData: RestoreData = { cash: 0 }) {
    super();
    console.log(restoreData);
    this._user = user;
    this._businesses = config.businesses.map(
      (businessConfig, index) =>
        new BusinessModel(
          businessConfig.name,
          {
            ...businessConfig,
            quantity: index === 0 ? 1 : 0,
          },
          restoreData.business?.find(({ name }) => name === businessConfig.name)
        )
    );
    this._cash = restoreData.cash;

    this._payoutThrottledSave = throttle(this.save.bind(this), 1000);
  }

  public get cash() {
    return this._cash;
  }

  public get businesses() {
    return this._businesses;
  }

  public getBusiness(businessName: string): BusinessModel {
    const business = this.businesses.find(({ name }) => businessName === name);
    if (business) {
      return business;
    } else {
      throw new Error(`Unknown Business ${businessName}`);
    }
  }

  public runBusiness(businessName: string) {
    const business = this.getBusiness(businessName);
    business.run();
  }

  public payout(businessName: string) {
    const business = this.getBusiness(businessName);
    business.finishedRunning();
    this.updateCash(this._cash + business.payout);
    this._payoutThrottledSave();
  }

  public updateCash(value: number) {
    this._cash = value;
    this.emit("cashUpdated", value);
  }

  purchase(businessName: string) {
    const business = this.getBusiness(businessName);
    if (this._cash >= business.cost) {
      business.purchase();
      this.updateCash(this._cash - business.cost);
      this.save();
    }
  }

  hireManager(businessName: string) {
    const business = this.getBusiness(businessName);
    if (this._cash >= business.managerCost) {
      business.managed = true;
      if (!business.running) {
        business.run();
      }
      this.updateCash(this._cash - business.managerCost);
      this.save();
    }
  }

  private save() {
    try {
      fetch("/api/save", {
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          user: this._user,
          cash: this.cash,
          business: this._businesses.map(({ name, managed, quantity }) => ({
            name,
            managed,
            quantity,
          })),
        }),
      });
    } catch (err) {
      console.error(`failed to save ${err}`);
    }
  }
}
