import * as PIXI from "pixi.js";
import gsap from "gsap";
import { PixiPlugin } from "gsap/PixiPlugin";
import GameService from "./services/GameService";
import BusinessList from "./components/BusinessList";
import Cash from "./components/Cash";

gsap.registerPlugin(PixiPlugin);
PixiPlugin.registerPIXI(PIXI);

class Game extends PIXI.Application {
  constructor(view: HTMLCanvasElement) {
    super({
      height: 720,
      width: 1280,
      autoStart: true,
      view,
    });

    const params = new URLSearchParams(window.location.search);
    const user = params.get("user") || "guest";
    const cashParam = params.get("cash");
    const cash = cashParam ? parseInt(cashParam) : 0;

    fetch(`/api/restore?user=${user}`)
      .then(async (response) => {
        const restoreData = await response.json();
        const isEmptyRestore = Object.keys(restoreData).length === 0;
        const gameService = new GameService(
          user,
          isEmptyRestore ? { cash } : restoreData
        );
        this.stage.addChild(new BusinessList({ gameService }));
        this.stage.addChild(new Cash({ gameService }));
      })
      .catch((err) => {
        console.error(err);
        alert("error fetching user data");
      });
  }
}

export default Game;
