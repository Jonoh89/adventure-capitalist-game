import * as PIXI from "pixi.js";
import GameService from "../services/GameService";
import BusinessModel from "../models/BusinessModel";
import Business from "./Business";

export default class BusinessList extends PIXI.Container {
  private _businesses: BusinessModel[];
  private _gameService: GameService;
  constructor({ gameService }: { gameService: GameService }) {
    super();

    this._gameService = gameService;
    this._businesses = gameService.businesses;
    this.layout();
  }

  private layout() {
    this._businesses.forEach((businessModel, index) => {
      const business = new Business({
        gameService: this._gameService,
        model: businessModel,
      });
      business.position.set(index < 5 ? 200 : 700, (index % 5) * 120 + 50);
      this.addChild(business);
    });
  }
}
