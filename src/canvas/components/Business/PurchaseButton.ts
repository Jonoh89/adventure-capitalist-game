import * as PIXI from "pixi.js";
import currencyFormatter from "../../helpers/currencyFormatter";

export default class PurchaseButton extends PIXI.Container {
  private _background: PIXI.Graphics;
  private _purchaseCostText: PIXI.Text;
  private _enabled: boolean = false;

  public constructor({
    purchaseCost,
    enable,
  }: {
    purchaseCost: number;
    enable: boolean;
  }) {
    super();
    this._background = this.addChild(this.createBackground(enable));
    this.position.set(100, 50);
    this.enabled = enable;

    const text = new PIXI.Text("Purchase", {
      fill: 0x000000,
      fontSize: 16,
    });
    text.anchor.set(0.5);
    text.position.set(this.width / 2, this.height / 4);
    this.addChild(text);

    this._purchaseCostText = new PIXI.Text(
      currencyFormatter.format(purchaseCost),
      { fill: 0x000000, fontSize: 16 }
    );
    this.updateCostWidth();
    this._purchaseCostText.anchor.set(0.5);
    this._purchaseCostText.position.set(this.width / 2, (this.height / 4) * 3);
    this.addChild(this._purchaseCostText);
  }

  private createBackground(enabled: boolean): PIXI.Graphics {
    const background = new PIXI.Graphics();
    background.beginFill(enabled ? 0xff9200 : 0x2b3940);
    background.drawRoundedRect(0, 0, 100, 40, 10);
    background.endFill();
    return background;
  }

  updateCost(cost: number) {
    this._purchaseCostText.text = currencyFormatter.format(cost);
    this.updateCostWidth();
  }

  updateCostWidth() {
    if (this._purchaseCostText.width > 90) {
      this._purchaseCostText.width = 90;
    }
  }

  set enabled(enabled: boolean) {
    if (enabled !== this._enabled) {
      this._enabled = enabled;
      this.interactive = enabled;
      this.buttonMode = enabled;
      this.removeChild(this._background);
      this._background = this.addChildAt(this.createBackground(enabled), 0);
    }
  }
}
