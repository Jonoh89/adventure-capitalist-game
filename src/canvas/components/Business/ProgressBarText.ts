import * as PIXI from "pixi.js";
import currencyFormatter from "../../helpers/currencyFormatter";

export default class ProgressBarText extends PIXI.Text {
  public constructor({ payout }: { payout: number }) {
    super(currencyFormatter.format(payout), {
      fill: 0x000000,
      fontSize: 30,
    });
    this.anchor.set(0.5);
    this.position.set(190, 30);
  }

  public updatePayout(payout: number) {
    this.text = currencyFormatter.format(payout);
  }
}
