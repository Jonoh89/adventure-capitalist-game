import * as PIXI from "pixi.js";
import currencyFormatter from "../../helpers/currencyFormatter";

export default class HireButton extends PIXI.Container {
  private _background: PIXI.Graphics | undefined;
  private _buttonText: PIXI.Text | undefined;
  private readonly _managerCost: number;

  public constructor({
    managerCost,
    enabled,
    managed,
  }: {
    managerCost: number;
    enabled: boolean;
    managed: boolean;
  }) {
    super();
    this._managerCost = managerCost;
    this.position.set(205, 50);
    this.enabled = !managed && enabled;

    this.update({ managed, enabled });
  }

  private createBackground(active: boolean): PIXI.Graphics {
    const background = new PIXI.Graphics();
    background.beginFill(active ? 0xff9200 : 0x2b3940);
    background.drawRoundedRect(0, 0, 80, 40, 10);
    background.endFill();
    return background;
  }

  set enabled(enabled: boolean) {
    this.interactive = enabled;
    this.buttonMode = enabled;
  }

  update({ managed, enabled }: { managed: boolean; enabled: boolean }) {
    this.removeChildren();
    if (managed) {
      this._background = this.addChild(this.createBackground(true));
      this._buttonText = this.addChild(this.createManagedText());
      this.enabled = false;
    } else if (enabled) {
      this._background = this.addChild(this.createBackground(true));
      this._buttonText = this.addChild(this.createManagerCostText());
      this.enabled = enabled;
    } else {
      this._background = this.addChild(this.createBackground(false));
      this._buttonText = this.addChild(this.createManagerCostText());
      this.enabled = enabled;
    }
  }

  private createManagedText(): PIXI.Text {
    const managedText = new PIXI.Text("💼", { fontSize: 20 });
    managedText.anchor.set(0.5);
    managedText.position.set(this.width / 2, this.height / 2);
    return managedText;
  }

  private createManagerCostText(): PIXI.Text {
    const managerCostText = new PIXI.Text(
      `Hire Manager\n ${currencyFormatter.format(this._managerCost)}`,
      { fill: 0x000000, fontSize: 16, align: "center" }
    );
    managerCostText.anchor.set(0.5);
    managerCostText.position.set(this.width / 2, this.height / 2);
    managerCostText.width = this.width - 10;
    return managerCostText;
  }
}
