import * as PIXI from "pixi.js";

export default class Background extends PIXI.Graphics {
  public constructor() {
    super();
    this.beginFill(0x9c9c9c);
    this.drawRoundedRect(0, 0, 300, 100, 20);
    this.endFill();
  }
}
