import * as PIXI from "pixi.js";

export default class ProgressBar extends PIXI.Graphics {
  public mask: PIXI.Graphics;

  public constructor() {
    super();
    this.beginFill(0x24ab00);
    this.drawRect(0, 0, 180, 40);
    this.endFill();
    this.position.set(100, 10);
    const mask = new PIXI.Graphics();
    mask.beginFill(0x000000);
    mask.drawRect(-180, 0, 180, 40);
    this.mask = mask;
    this.addChild(mask);
    mask.x = 0;
  }
}
