import * as PIXI from "pixi.js";

export default class RunButton extends PIXI.Graphics {
  private _quantityText: PIXI.Text;
  private _managed: boolean;

  public constructor({
    managed,
    quantity,
  }: {
    managed: boolean;
    quantity: number;
  }) {
    super();
    this._managed = managed;
    this.beginFill(0xffffff);
    this.drawCircle(0, 0, 40);
    this.endFill();
    this.position.set(50, 50);

    this._quantityText = new PIXI.Text(quantity.toString(), {
      fill: 0x000000,
      fontSize: 20,
    });
    this._quantityText.anchor.set(0.5);
    this.addChild(this._quantityText);
    this.updateQuantity(quantity);
  }

  updateQuantity(quantity: number) {
    this._quantityText.text = quantity.toString();
    this.enabled = quantity > 0;
  }

  set enabled(value: boolean) {
    const shouldEnable = !this._managed && value;
    this.interactive = shouldEnable;
    this.buttonMode = shouldEnable;
  }

  set managed(value: boolean) {
    this._managed = value;
    this.enabled = false;
  }
}
