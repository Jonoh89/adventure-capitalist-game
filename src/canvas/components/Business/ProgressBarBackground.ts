import * as PIXI from "pixi.js";

export default class ProgressBarBackground extends PIXI.Graphics {
  public constructor() {
    super();
    this.lineStyle(1, 0x24ab00);
    this.drawRect(0, 0, 180, 40);
    this.position.set(100, 10);
  }
}
