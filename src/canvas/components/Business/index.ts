import * as PIXI from "pixi.js";
import gsap from "gsap";
import GameService from "../../services/GameService";
import BusinessModel from "../../models/BusinessModel";
import Background from "./Background";
import RunButton from "./RunButton";
import ProgressBar from "./ProgressBar";
import PurchaseButton from "./PurchaseButton";
import ProgressBarBackground from "./ProgressBarBackground";
import ProgressBarText from "./ProgressBarText";
import NameText from "./NameText";
import HireButton from "./HireButton";

export default class Index extends PIXI.Container {
  private _model: BusinessModel;
  private _gameService: GameService;
  private _runButton: RunButton;
  private _progressBarBackground: ProgressBarBackground;
  private _purchaseButton: PurchaseButton;
  private _progressBar: ProgressBar;
  private _progressBarText: ProgressBarText;
  private _hireButton: HireButton;

  constructor({
    model,
    gameService,
  }: {
    model: BusinessModel;
    gameService: GameService;
  }) {
    super();
    this._gameService = gameService;
    this._model = model;
    gameService.on("cashUpdated", (cash: number) => this.onCashUpdated(cash));
    this._model.on("run", this.run.bind(this));
    this._model.on("purchased", this.onPurchased.bind(this));
    this._model.on("managed", this.onManaged.bind(this));

    this.addChild(new Background());
    this.addChild(new NameText({ name: this._model.name }));

    this._runButton = this.addChild(
      new RunButton({
        managed: this._model.managed,
        quantity: this._model.quantity,
      })
    );
    this._progressBarBackground = this.addChild(new ProgressBarBackground());
    this._purchaseButton = this.addChild(
      new PurchaseButton({
        purchaseCost: this._model.cost,
        enable: this._gameService.cash >= this._model.cost,
      })
    );
    this._progressBar = this.addChild(new ProgressBar());
    this._progressBarText = this.addChild(
      new ProgressBarText({ payout: model.payout })
    );
    this._hireButton = this.addChild(
      new HireButton({
        managed: this._model.managed,
        managerCost: this._model.managerCost,
        enabled: this._gameService.cash >= this._model.managerCost,
      })
    );

    this.registerEventListeners();

    if (this._model.managed) this.run();
  }

  private registerEventListeners() {
    this._runButton.on("click", () => {
      if (!this._model.running && this._model.quantity > 0) {
        this._gameService.runBusiness(this._model.name);
      }
    });

    this._purchaseButton.on("click", () =>
      this._gameService.purchase(this._model.name)
    );

    this._hireButton.on("click", () => {
      this._gameService.hireManager(this._model.name);
    });
  }

  private run() {
    gsap.to(this._progressBar.mask, {
      duration: this._model.secondsToPayout,
      pixi: { x: 180 },
      ease: "linear",
      onComplete: this.onBusinessRunComplete.bind(this),
    });
  }

  private onPurchased() {
    this._runButton.updateQuantity(this._model.quantity);
    this._progressBarText.updatePayout(this._model.payout);
    this._purchaseButton.updateCost(this._model.cost);
  }

  private onBusinessRunComplete() {
    this._progressBar.mask.x = 0;
    this._gameService.payout(this._model.name);
    if (this._model.managed) {
      this.run();
    }
  }

  private onCashUpdated(cash: number) {
    this._purchaseButton.enabled = cash >= this._model.cost;
    this._hireButton.update({
      managed: this._model.managed,
      enabled: cash >= this._model.managerCost,
    });
  }

  private onManaged() {
    this._hireButton.update({ managed: true, enabled: false });
    this._runButton.managed = true;
  }
}
