import * as PIXI from "pixi.js";

export default class NameText extends PIXI.Text {
  constructor({ name }: { name: string }) {
    super(name, { fill: 0xee0500 });
    this.anchor.set(0.5);
    this.position.set(150, -5);
  }
}
