import * as PIXI from "pixi.js";
import GameService from "../services/GameService";
import currencyFormatter from "../helpers/currencyFormatter";

export default class Cash extends PIXI.Text {
  public constructor({ gameService }: { gameService: GameService }) {
    super(currencyFormatter.format(gameService.cash), {
      fill: 0xffffff,
      fontSize: 40,
    });

    this.x = 640;
    this.y = 30;
    this.anchor.set(0.5);

    gameService.on("cashUpdated", (cash: number) => {
      this.text = currencyFormatter.format(cash);
    });
  }
}
