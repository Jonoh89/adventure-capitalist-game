import EventEmitter from "eventemitter3";
import { RestoreBusinessData } from "../services/GameService";

interface BusinessData {
  cost: number;
  secondsToPayout: number;
  quantity: number;
  managerCost: number;
}

export default class BusinessModel extends EventEmitter {
  public readonly name: string;
  public readonly secondsToPayout: number;
  private readonly _cost: number;
  private readonly _managerCost: number;
  private _running: boolean = false;
  private _quantity: number = 0;
  private _managed: boolean = false;

  constructor(
    name: string,
    businessData: BusinessData,
    restoreData?: RestoreBusinessData
  ) {
    super();
    this.name = name;
    this.secondsToPayout = businessData.secondsToPayout;
    this._cost = businessData.cost;
    this._quantity = businessData.quantity;
    this._managerCost = businessData.managerCost;
    if (restoreData) {
      this._managed = restoreData.managed;
      this._quantity = restoreData.quantity;
    }
  }

  get running() {
    return this._running;
  }

  get quantity() {
    return this._quantity;
  }

  get payout() {
    return (this._cost / this.secondsToPayout) * this.quantity;
  }

  get cost() {
    return this._cost + this._cost * this.quantity * 0.1;
  }

  get managed() {
    return this._managed;
  }

  get managerCost() {
    return this._managerCost;
  }

  set managed(value) {
    this._managed = value;
    this.emit("managed");
  }

  run() {
    this._running = true;
    this.emit("run");
  }

  purchase() {
    this._quantity += 1;
    this.emit("purchased");
  }

  finishedRunning() {
    this._running = false;
  }
}
