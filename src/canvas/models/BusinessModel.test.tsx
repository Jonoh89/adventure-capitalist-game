import BusinessModel from "./BusinessModel";

describe("Business Model", () => {
  let business: BusinessModel;

  describe("constructor", () => {
    beforeEach(() => {
      business = new BusinessModel("Pub", {
        cost: 4,
        secondsToPayout: 10,
        quantity: 0,
        managerCost: 1000,
      });
    });

    it("should set the public name", () => {
      expect(business.name).toEqual("Pub");
    });

    it("should set the public cost", () => {
      expect(business.cost).toEqual(4);
    });

    it("should set the secondsToPayout", () => {
      expect(business.secondsToPayout).toEqual(10);
    });

    it("should default to running false", () => {
      expect(business.running).toEqual(false);
    });

    it("should set the manager cost", () => {
      expect(business.managerCost).toEqual(1000);
    });

    it("should default to unmanaged", () => {
      expect(business.managed).toEqual(false);
    });

    it("should set the quantity", () => {
      business = new BusinessModel("Pub", {
        cost: 4,
        secondsToPayout: 10,
        quantity: 2,
        managerCost: 1000,
      });
      expect(business.quantity).toEqual(2);
    });

    it("should ", () => {});
  });

  describe("run", () => {
    beforeEach(() => {
      jest.spyOn(business, "emit");
      business.run();
    });
    it("should set running to true", () => {
      expect(business.running).toEqual(true);
    });

    it("should emit run", () => {
      expect(business.emit).toHaveBeenCalledWith("run");
    });
  });

  describe("payout", () => {
    it("should calculate the payout as cost / payout seconds", () => {
      business = new BusinessModel("Pub", {
        cost: 40000,
        secondsToPayout: 10,
        quantity: 1,
        managerCost: 1000,
      });
      expect(business.payout).toEqual(4000);

      business = new BusinessModel("Chippy", {
        cost: 500,
        secondsToPayout: 1,
        quantity: 1,
        managerCost: 1000,
      });
      expect(business.payout).toEqual(500);
    });

    it("should multiply the payout by the quantity", () => {
      business = new BusinessModel("Pub", {
        cost: 200,
        secondsToPayout: 1,
        quantity: 1,
        managerCost: 1000,
      });
      business.purchase();
      business.purchase();
      expect(business.payout).toEqual((200 / business.secondsToPayout) * 3);
    });
  });

  describe("cost", () => {
    it("should return the cost based on quantity owned", () => {
      business = new BusinessModel("Lemonade Stand", {
        cost: 1,
        secondsToPayout: 2,
        quantity: 0,
        managerCost: 1000,
      });
      expect(business.cost).toEqual(1);
      business.purchase();
      expect(business.cost).toEqual(1.1);
      business.purchase();
      expect(business.cost).toEqual(1.2);
    });
  });

  describe("purchase", () => {
    it("should increase the quantity", () => {
      business = new BusinessModel("Pub", {
        cost: 40000,
        secondsToPayout: 10,
        quantity: 1,
        managerCost: 1000,
      });
      business.purchase();
      expect(business.quantity).toEqual(2);
      business.purchase();
      business.purchase();
      expect(business.quantity).toEqual(4);
    });

    it("should emit purchased when one has been purchased", () => {
      const stub = jest.fn();
      business = new BusinessModel("Pub", {
        cost: 40000,
        secondsToPayout: 10,
        quantity: 1,
        managerCost: 1000,
      });
      business.once("purchased", stub);
      business.purchase();
      expect(stub).toHaveBeenCalled();
    });
  });

  describe("finishedRunning", () => {
    it("should set running to false", () => {
      business = new BusinessModel("Lemonade Stand", {
        cost: 1,
        secondsToPayout: 2,
        quantity: 1,
        managerCost: 1000,
      });
      expect(business.running).toEqual(false);
      business.run();
      expect(business.running).toEqual(true);
      business.finishedRunning();
      expect(business.running).toEqual(false);
    });
  });
});
