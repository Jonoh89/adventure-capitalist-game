const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("en-GB", {
  style: "currency",
  currency: "GBP",
});

export default currencyFormatter;
