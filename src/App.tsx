import React, { useState, useEffect, useRef } from "react";
import Game from "./canvas/Game";
import "./App.css";
import useCurrentWidth from "./hooks/useCurrentWidth";
import useCurrentHeight from "./hooks/useCurrentHeight";
import calculateLargestDimensions from "./helpers/calculateLargestDimensions";

function App() {
  const [game, setGame] = useState();
  const canvas = useRef<HTMLCanvasElement>(null);
  const currentWidth = useCurrentWidth();
  const currentHeight = useCurrentHeight();
  const { width, height } = calculateLargestDimensions(
    currentWidth,
    currentHeight
  );

  useEffect(() => {
    if (!game && canvas?.current) {
      setGame(new Game(canvas.current));
      // @ts-ignore
      window.game = game;
    }
  }, [game]);

  return (
    <div className="App">
      <canvas className="Game" ref={canvas} style={{ width, height }} />
    </div>
  );
}

export default App;
