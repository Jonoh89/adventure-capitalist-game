module.exports = [
  {
    name: "Lemonade Stand",
    cost: 1,
    secondsToPayout: 1,
    managerCost: 1000,
  },
  {
    name: "Newspaper Delivery",
    cost: 60,
    secondsToPayout: 3,
    managerCost: 15000,
  },
  {
    name: "Car Wash",
    cost: 500,
    secondsToPayout: 8,
    managerCost: 100000,
  },
  {
    name: "Pizza Delivery",
    cost: 1000,
    secondsToPayout: 12,
    managerCost: 500000,
  },
  {
    name: "Chip Shop",
    cost: 100000,
    secondsToPayout: 24,
    managerCost: 10000000,
  },
  {
    name: "Pub",
    cost: 1250000,
    secondsToPayout: 48,
    managerCost: 10000000,
  },
  {
    name: "Football Team",
    cost: 15000000,
    secondsToPayout: 96,
    managerCost: 111111111,
  },
  {
    name: "Movie Studio",
    cost: 180000000,
    secondsToPayout: 192,
    managerCost: 555555555,
  },
  {
    name: "Bank",
    cost: 2200000000,
    secondsToPayout: 384,
    managerCost: 1000000000,
  },
  {
    name: "Oil Company",
    cost: 25000000000,
    secondsToPayout: 768,
    managerCost: 10000000000,
  },
];
