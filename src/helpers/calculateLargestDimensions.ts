export default function calculateLargestDimensions(
  currentWidth: number,
  currentHeight: number
) {
  let width, height;
  if (currentHeight > (currentWidth / 16) * 9) {
    width = currentWidth;
    height = (currentWidth / 16) * 9;
  } else {
    width = (currentHeight / 9) * 16;
    height = currentHeight;
  }

  return { width, height };
}
