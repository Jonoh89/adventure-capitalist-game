const businessesConfig = require("../../src/config/businesses");

module.exports = function calculateGeneratedCash(state) {
  const secondsPassed =
    (new Date().getTime() - new Date(state.updatedAt).getTime()) / 1000;

  return state.business.reduce((total, nextBusiness) => {
    if (!nextBusiness.managed) return total;

    const businessConfig = businessesConfig.find(
      (business) => business.name === nextBusiness.name
    );
    const earned =
      Math.floor(secondsPassed / businessConfig.secondsToPayout) *
      (businessConfig.cost / businessConfig.secondsToPayout) *
      nextBusiness.quantity;
    return total + earned;
  }, 0);
};
