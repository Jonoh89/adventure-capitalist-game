const restoreHandler = require("./restoreHandler");

module.exports = function createRestoreHandler(db) {
  return (res, req) => restoreHandler({ db })(res, req);
};
