const calculateGeneratedCash = require("./calculateGeneratedCash");

module.exports = ({ db }) =>
  async function restoreHandler(req, res) {
    try {
      const userQuery = { user: req.query.user };
      const state = db.states.findOne(userQuery);
      if (state) {
        state.cash += calculateGeneratedCash(state);
      }
      res.send(state || {});
    } catch (error) {
      console.error(error);
      res.status(500).send({ error });
    }
  };
