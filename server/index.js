const express = require("express");
const bodyParser = require("body-parser");
const diskdb = require("diskdb");
const app = express();
const createRestoreHandler = require("./restore/createRestoreHandler");
const createSaveHandler = require("./save/createSaveHandler");
const port = 4000;

// parse application/json
app.use(bodyParser.json());

const db = diskdb.connect("./server/data/db", ["states"]);

app.get("/api/restore", createRestoreHandler(db));
app.post("/api/save", createSaveHandler(db));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
