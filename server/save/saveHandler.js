module.exports = ({ db }) =>
  async function searchHandler(req, res) {
    try {
      const userQuery = { user: req.body.user };
      const newState = { ...req.body, updatedAt: new Date() };
      const state = db.states.findOne(userQuery);
      if (state) {
        db.states.update(userQuery, newState);
      } else {
        db.states.save(newState);
      }
      res.send({ success: true });
    } catch (error) {
      console.error(error);
      res.status(500).send({ error });
    }
  };
