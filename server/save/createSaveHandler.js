const saveHandler = require("./saveHandler");

module.exports = function createSearchHandler(db) {
  return (res, req) => saveHandler({ db })(res, req);
};
