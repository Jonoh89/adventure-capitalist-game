# Adventure Capitalist Game

This is a full stack adventure capitalist game. It allows players to build up their cash by running business and automate their income by hiring managers to run them while they are away.

This is a full stack app and requires you to start the server before the client. You should run the following:
<br />`yarn install`
<br />`yarn server`
<br />`yarn start`
<br />Then navigate to : `http://localhost:3000?user=yourName`
<br />You can also cheat to get starting cash with: `http://localhost:3000?user=yourName&cash=100000000`

## Technical Decisions

My idea was to go full stack to implement in away that could remember your progress across devices.
The current solution saves every time a major decision is made, or a maximum of once every second when a payout is performed.
The server then saves current progress with the current datetime and uses that to calculate the progress on restore.

### Current issues

- it sends a lot of requests, may be better to use websockets
- it doesn't send the current businesses progress - it works out as if they had no progress and restarts them from 0
- You could have two sessions overriding eachother - should block that from happening
- User is just read from the query string, should be some auth service

### Frameworks

#### Pixi

Pixi is what I use for work and have done for ~5 years. It offers you a lightweight canvas framework
to build your own games using just what you need. As you will see it is currently ugly, I used no
assets only PIXI graphics and text, not being an artist, I like to use these to prototype ideas and sizes.

#### React

I am familiar with react and creat-react-app is my go-to to get set up with lots of my favourite tools.
Although maybe overkill here my intention would be use it to show errors thrown from the canvas
and handle positioning the canvas.

#### Express

Express is the simplest and quickest way to get a node server working. Using node i was able to
share the business config between client and server. I would have liked to use typescript given time to set it up.

#### diskdb

I wanted a simple nosql database to throw the data in. I googled and found this, it just stores
the data in a readable json file locally, great for prototyping.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn server`

Starts he server

### `yarn prettier`

Formats the code in a standard style
